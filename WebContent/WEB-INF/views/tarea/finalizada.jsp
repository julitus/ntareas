<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<td>${tarea.id}</td>
<td>${tarea.description}</td>
<td>Finalizado</td>
<td>
	<fmt:formatDate value="${tarea.dataFinalized.time}" pattern="dd/MM/yyyy" />
</td>
<td><a href="remueveTarea?id=${tarea.id}">Remover</a></td>
<td><a href="muestraTarea?id=${tarea.id}">Editar</a></td>