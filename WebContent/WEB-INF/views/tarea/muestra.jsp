<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
		<link type="text/css" href="resources/css/tareas.css" rel="stylesheet" />
	</head>
	<body>
		<h3>Edita tarea - ${tarea.id}</h3>
		<form action="editaTarea" method="post">
			
			<input type="hidden" name="id" value="${tarea.id}" />
			
			Descripcion: <br />
			<textarea name=description rows="5" cols="100">${tarea.description}</textarea><br />
			Finalizado?
			<input type="checkbox" name="finalized" 
				value="true" ${tarea.finalized? 'checked' : '' } /><br />
			<input type="text" name="dataFinalized"
				value="<fmt:formatDate
				value="${tarea.dataFinalized.time}"
				pattern="dd/MM/yyyy" />" /><br />
			<input type="submit" value="Editar">
		</form>
	</body>
</html>