<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>

	<head>
		<script type="text/javascript" src="resources/js/jquery-1.10.2.js"></script>
		<link type="text/css" href="resources/css/tareas.css" rel="stylesheet" />
	</head>
	
	<body>
		
		<script type="text/javascript">
			function finalizarAhora(id){
				$.post("finalizaTarea", {'id': id}, function(respuesta){
					$("#tarea_"+id).html(respuesta);
				});
			}
		</script>
		
		<a href="nuevaTarea">Nueva Tarea</a>
		<a href="menu">Ir al Menu</a>
		<br /><br />
		
		<table>
			<tr>
				<th>ID</th>
				<th>Descripcion</th>
				<th>Finalizado?</th>
				<th>Fecha de Finalizacion</th>
				<th colspan="2">ACCION</th>
			</tr>
			<c:forEach items="${tareas}" var="tarea">
				<tr id="tarea_${tarea.id}">
					<td>${tarea.id}</td>
					<td>${tarea.description}</td>
					<c:if test="${tarea.finalized eq false}">
						<td>
							<a href="#" onClick="finalizarAhora(${tarea.id})">Finalizar ahora!</a>
						</td>
					</c:if>
					<c:if test="${tarea.finalized eq true}">
						<td>Finalizado</td>
					</c:if>
					<td>
						<fmt:formatDate
							value="${tarea.dataFinalized.time}"
							pattern="dd/MM/yyyy"/>
					</td>
					<td><a href="remueveTarea?id=${tarea.id}">Remover</a></td>
					<td><a href="muestraTarea?id=${tarea.id}">Editar</a></td>
				</tr>
			</c:forEach>
		</table>
		
	</body>
</html>