<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>    
    
<html>
	<head>
		<link type="text/css" href="resources/css/tareas.css" rel="stylesheet" />
	</head>
	<body>
		<h3>Adiciona Tareas</h3>
		<form action="adicionaTarea" method="post">
			Descripcion: <br />
			<textarea name=description rows="5" cols="100"></textarea><br />
			<input type="submit" value="Adicionar">
		</form>
		<form:errors path="tarea.description" cssStyle="color: red"/>
	</body>
</html>