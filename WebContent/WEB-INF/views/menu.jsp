<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html>
	<head>
		<link type="text/css" href="resources/css/tareas.css" rel="stylesheet" />
	</head>
	<body>
		<h2>Pagina inicial de Lista de Tareas</h2>
		<p>Bienvenido, ${usuarioLogueado.login}</p>
		<a href="listaTareas">Click aqui</a> para acceder a lista de Tareas o 
		<a href="logout">Salir</a> del Sistema.
	</body>
</html>