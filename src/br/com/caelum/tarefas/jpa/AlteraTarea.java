package br.com.caelum.tarefas.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.caelum.tarefas.modelo.Tarea;

public class AlteraTarea {

	public static void main(String[] args) {

		Tarea tarea = new Tarea();
		tarea.setId(2L);
		tarea.setDescription("Estudiar JPA e Hibernate");
		tarea.setFinalized(false);
		tarea.setDataFinalized(null);

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("tareas");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		manager.merge(tarea);
		manager.getTransaction().commit();

	}

}
