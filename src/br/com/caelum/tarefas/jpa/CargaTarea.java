package br.com.caelum.tarefas.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.caelum.tarefas.modelo.Tarea;

public class CargaTarea {

	public static void main(String[] args) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("tareas");
		EntityManager manager = factory.createEntityManager();
		
		Tarea encontrada = manager.find(Tarea.class, 1L);
		System.out.println(encontrada.getDescription());
		
		manager.close();

	}

}
