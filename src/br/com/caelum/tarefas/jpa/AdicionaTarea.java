package br.com.caelum.tarefas.jpa;

import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.caelum.tarefas.modelo.Tarea;

public class AdicionaTarea {

	public static void main(String[] args) {
		
		Tarea tarea = new Tarea();
		tarea.setDescription("Katy chimpandolfa");
		tarea.setFinalized(false);
		tarea.setDataFinalized(Calendar.getInstance());
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("tareas");
		EntityManager manager = factory.createEntityManager();
		
		manager.getTransaction().begin();
		manager.persist(tarea);
		manager.getTransaction().commit();
		
		System.out.println("ID de Tarea: " + tarea.getId());
		
		manager.close();
		
	}

}
