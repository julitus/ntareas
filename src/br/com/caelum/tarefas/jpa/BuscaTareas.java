package br.com.caelum.tarefas.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.caelum.tarefas.modelo.Tarea;

public class BuscaTareas {

	public static void main(String[] args) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("tareas");
		EntityManager manager = factory.createEntityManager();
		
		Query query = manager.createQuery("select t from Tarea as t " +
				"where t.finalized = :paramFinalized");
		query.setParameter("paramFinalized", false);
		
		List<Tarea> lista = query.getResultList();
		
		for (Tarea t : lista){
			System.out.println(t.getDescription()+ " - " +t.getId()+ " - " +t.isFinalized());
		}
		
		manager.close();
	}

}
