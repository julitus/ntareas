package br.com.caelum.tarefas.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/*import br.com.caelum.tarefas.ConnectionFactory;*/
import br.com.caelum.tarefas.modelo.Tarea;

@Repository
public class JdbcTareaDao {
	
	private final Connection conexion;
	
	@Autowired
	public JdbcTareaDao(DataSource dataSource) {
		try {
			this.conexion = dataSource.getConnection();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void adiciona(Tarea tarea) {
		String sql = "insert into tareas (description, finalized) values (?,?)";
		PreparedStatement stmt;
		try {
			stmt = conexion.prepareStatement(sql);
			stmt.setString(1, tarea.getDescription());
			stmt.setBoolean(2, tarea.isFinalized());
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void edita(Tarea tarea) {
		String sql = "update tareas set description = ?, finalized = ?, dataFinalized = ? where id = ?";
		PreparedStatement stmt;
		try {
			stmt = conexion.prepareStatement(sql);
			stmt.setString(1, tarea.getDescription());
			stmt.setBoolean(2, tarea.isFinalized());
			stmt.setDate(3, tarea.getDataFinalized() != null ? new Date(
					tarea.getDataFinalized().getTimeInMillis()) : null);
			stmt.setLong(4, tarea.getId());
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void remueve(Tarea tarea) {

		if (tarea.getId() == null) {
			throw new IllegalStateException("Id de la tarea no debe ser nula.");
		}

		String sql = "delete from tareas where id = ?";
		PreparedStatement stmt;
		try {
			stmt = conexion.prepareStatement(sql);
			stmt.setLong(1, tarea.getId());
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Tarea> lista() {
		try {
			List<Tarea> tareas = new ArrayList<Tarea>();
			PreparedStatement stmt = this.conexion
					.prepareStatement("select * from tareas");

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				tareas.add(rellenaTarea(rs));
			}

			rs.close();
			stmt.close();

			return tareas;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Tarea buscaPorId(Long id) {

		if (id == null) {
			throw new IllegalStateException("Id de la tarea no debe ser nula.");
		}

		try {
			PreparedStatement stmt = this.conexion
					.prepareStatement("select * from tareas where id = ?");
			stmt.setLong(1, id);

			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				return rellenaTarea(rs);
			}

			rs.close();
			stmt.close();

			return null;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	private Tarea rellenaTarea(ResultSet rs) throws SQLException {
		Tarea tarea = new Tarea();

		tarea.setId(rs.getLong("id"));
		tarea.setDescription(rs.getString("description"));
		tarea.setFinalized(rs.getBoolean("finalized"));

		Date data = rs.getDate("dataFinalized");
		if (data != null) {
			Calendar dataFinalized = Calendar.getInstance();
			dataFinalized.setTime(data);
			tarea.setDataFinalized(dataFinalized);
		}
		return tarea;
	}
	
	public void finaliza(Long id) {

		if (id == null) {
			throw new IllegalStateException("Id de la tarea no debe ser nula.");
		}

		String sql = "update tareas set finalized = ?, dataFinalized = ? where id = ?";
		PreparedStatement stmt;
		try {
			stmt = conexion.prepareStatement(sql);
			stmt.setBoolean(1, true);
			stmt.setDate(2, new Date(Calendar.getInstance().getTimeInMillis()));
			stmt.setLong(3, id);
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
