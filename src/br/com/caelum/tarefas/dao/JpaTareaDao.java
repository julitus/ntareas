package br.com.caelum.tarefas.dao;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.caelum.tarefas.modelo.Tarea;

@Repository
public class JpaTareaDao implements TareaDao{
	
	@PersistenceContext
	EntityManager manager;
	
	public void adiciona(Tarea tarea){
		manager.persist(tarea);
	}
	
	public void edita(Tarea tarea){
		manager.merge(tarea);
	}
	
	public List<Tarea> lista(){
		return manager.createQuery("select t from Tarea t").getResultList();
	}
	
	public Tarea buscaPorId(Long id){
		return manager.find(Tarea.class, id);
	}
	
	public void remueve(Tarea tarea){
		Tarea tareaARemover = buscaPorId(tarea.getId());
		manager.remove(tareaARemover);
	}
	
	public void finaliza(Long id){
		Tarea tarea = buscaPorId(id);
		tarea.setFinalized(true);
		tarea.setDataFinalized(Calendar.getInstance());
	}
}
