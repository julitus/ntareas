package br.com.caelum.tarefas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/*import br.com.caelum.tarefas.ConnectionFactory;*/
import br.com.caelum.tarefas.modelo.Usuario;

@Repository
public class JdbcUsuarioDao {
	
	private Connection conexion;
	
	@Autowired
	public JdbcUsuarioDao(DataSource dataSource) {
		try {
			conexion = dataSource.getConnection();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean existeUsuario(Usuario usuario) {
		
		if(usuario == null) {
			throw new IllegalArgumentException("Usuario no debe ser nulo.");
		}
		
		try {
			PreparedStatement stmt = this.conexion.prepareStatement("select * from usuarios where login = ? and pass = ?");
			stmt.setString(1, usuario.getLogin());
			stmt.setString(2, usuario.getPass());
			ResultSet rs = stmt.executeQuery();

			boolean encontrado = rs.next();
			rs.close();
			stmt.close();

			return encontrado;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}

