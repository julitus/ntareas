package br.com.caelum.tarefas.dao;

import java.util.List;

import br.com.caelum.tarefas.modelo.Tarea;

public interface TareaDao {
	Tarea buscaPorId(Long id);
	List<Tarea> lista();
	void adiciona(Tarea t);
	void edita(Tarea t);
	void remueve(Tarea t);
	void finaliza(Long id);
}
