package br.com.caelum.tarefas.controller;

/*import javax.servlet.http.HttpServletResponse;*/
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.caelum.tarefas.dao.JdbcTareaDao;
import br.com.caelum.tarefas.dao.TareaDao;
import br.com.caelum.tarefas.modelo.Tarea;

@Transactional
@Controller
public class TareaController {
	
	/*private final JdbcTareaDao dao;*/
	@Autowired
	TareaDao dao;
	
	/*@Autowired
	public TareaController(JdbcTareaDao dao){
		this.dao = dao;
	}*/
	
	@RequestMapping("nuevaTarea")
	public String form(){
		return "tarea/formulario";
	}
	
	@RequestMapping("adicionaTarea")
	public String adiciona(@Valid Tarea tarea, BindingResult result){
		
		if(result.hasFieldErrors("description")){
			return "tarea/formulario";
		}
		dao.adiciona(tarea);
		return "tarea/adicionada";
	}
	
	@RequestMapping("editaTarea")
	public String edita(Tarea tarea){
		dao.edita(tarea);
		return "redirect:listaTareas";
	}
	
	@RequestMapping("remueveTarea")
	public String remueve(Tarea tarea){
		dao.remueve(tarea);
		return "redirect:listaTareas";
	}
	
	@RequestMapping("muestraTarea")
	public String muestra(Long id, Model model){
		model.addAttribute("tarea", dao.buscaPorId(id));
		return "tarea/muestra";
	}
	
	@RequestMapping("listaTareas")
	public String lista(Model model){
		
		model.addAttribute("tareas", dao.lista());
		return "tarea/lista";
	}
	
	/*@RequestMapping("finalizaTarea")
	public void finaliza(Long id, HttpServletResponse response){
		dao.finaliza(id);
		response.setStatus(200);
	}*/
	
	@RequestMapping("finalizaTarea")
	public String finaliza(Long id, Model model){
		dao.finaliza(id);
		model.addAttribute("tarea", dao.buscaPorId(id));
		return "tarea/finalizada";
	}
	
}
